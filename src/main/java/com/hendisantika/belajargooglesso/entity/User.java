package com.hendisantika.belajargooglesso.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : belajar-google-sso
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/11/18
 * Time: 22.36
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "s_user")
@Data
public class User {

    @Id
    private String id;

    private String username;

    @ManyToOne
    @JoinColumn(name = "id_role")
    private Role role;

}