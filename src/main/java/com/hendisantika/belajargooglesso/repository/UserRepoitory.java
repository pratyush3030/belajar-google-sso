package com.hendisantika.belajargooglesso.repository;

import com.hendisantika.belajargooglesso.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : belajar-google-sso
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/11/18
 * Time: 22.37
 * To change this template use File | Settings | File Templates.
 */
public interface UserRepoitory extends PagingAndSortingRepository<User, String> {
    User findByUsername(String username);
}
