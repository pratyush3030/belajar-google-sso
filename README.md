# Belajar Google SSO

Run this project by this command : `mvn clean spring-boot:run`

### Screenshot

Login Page

![Login Page](img/login.png "Login Page")

Google SSO Page

![Google SSO Page](img/sso.png "Google SSO Page")

Home Page

![Home Page](img/home.png "Home Page")